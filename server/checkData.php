<?php

//Include our class to insert and get data from SQLite Database
include_once("SQliteDB.php");
include_once("api.php");

//Function which checks if all brackets are closed properly or not
function brackets($braces) {

    $bracketArrOptions = [ '[' => ']', '{' => '}', '(' => ')', '<' => '>'];
    $bracketArrOptionsFlipped = array_flip($bracketArrOptions);

    $bracesConvertedArray = str_split($braces);
    $stack = [];

    foreach ($bracesConvertedArray as $currentChar) {

        if (isset($bracketArrOptions[$currentChar])) {
            $stack[] = $bracketArrOptions[$currentChar];

        } else if (isset($bracketArrOptionsFlipped[$currentChar])) {
            
            $expectedChar = array_pop($stack); 

            if (($expectedChar === NULL) || ($currentChar != $expectedChar)) {
                return false;
            }
        }

    }
   
    return empty($stack);
}

$formQuery = trim($_REQUEST['s']);

//Collect that we need to insert in database and send back.
$bracketsCheck = brackets($formQuery); 

$db = new SQliteDB('brackets_query_history.db', 'queries_table');

$db->insertData($formQuery, $bracketsCheck);

//Send back answer
$json = json_encode(array("success" => $bracketsCheck));

$outputJson = checkJsonErrors($json);

echo $outputJson;

header("Content-type:application/json");

?>