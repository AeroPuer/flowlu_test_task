function showModalWindow(message, context){
  
    var bracketCheckContainer = $(context).closest(".brackets_check_container");

    //Check for opened modal windows before open one more
    var openedModalWindows = $(bracketCheckContainer).find(".modal.fade.show");

    if(openedModalWindows.length>0){
        closeModalWindow(context);
    }

    $(bracketCheckContainer).find(".modal-body").text(message);
    $(bracketCheckContainer).find(".modal.fade").modal();
    $(bracketCheckContainer).find(".modal.fade").addClass("show");
}

function closeModalWindow(context){

    var bracketCheckContainer = $(context).closest(".brackets_check_container");
    $(bracketCheckContainer).find(".modal.fade").removeClass("show");
    
}

function escapeHtml (data) {

    var entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
        };

    return String(data).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });

}

//Check that string isn't empty
function isEmpty(str){
    return (str.length == 0 || !str.trim())
}


var bracketCheck = (function () {
 
    return {

        sendData: function() {

            //Save the context - in order not to lose it
            var _this = this;

            //Находим корневый div компонента
            var bracketCheckContainer = $(_this).closest(".brackets_check_container");

            var query = bracketCheckContainer.find(".queryInput").val();

            if(isEmpty(query)){
                showModalWindow("Ошибка вы пытаетесь отправить пустое поле!", _this);
                return;
            }

            //Clean input
            bracketCheckContainer.find(".queryInput").val('');

            $.ajax({
                type: "POST",
                url: "server/checkData.php",
                data: `s=${query}`,
                success: function(data){
                    if(data.success == true){
                        showModalWindow("Данные корректны - проверка успешно пройдена", _this);
                    }else{
                        showModalWindow("Данные некорректны - проверка не пройдена", _this);
                    }

                },
                
                error: function(){
                    showModalWindow(`Возникла ошибка с отправкой данных ${query}`, _this);
                }
            }); 
          
        },
        
        getData: function() {

            console.log("get Data");
            var _this = this;
       
            //find the root div brackets_check_container element
            var bracketCheckContainer = $(_this).closest(".brackets_check_container");
            var queryResultContainer = bracketCheckContainer.find(".queryResult");
       
           $.ajax({
               type: "GET",
               url: "server/getResults.php",
               success: function(data){
                  
       
                   if(data.length == 0 || data == undefined){
                       var errorText = "<div class='alert alert-danger' role='alert'>Данных в базе данных пока нет либо возникла непредвиденная ошибка - обратитесь к администратору</div>"
                       queryResultContainer.html(errorText);
                       return;
                   }
       
                   var tableQueries =  $('<table>', {
                       class: 'table'
                     });
       
                   var tableTitle = $('<thead><tr><th class="row">Номер запроса</th><th>Запрос</th><th>Результат запроса</th></tr></thead>');
                   tableTitle.appendTo(tableQueries);
       
                   data.forEach(function(element, key) {
                       var dataRows = $('<tr><td>'+ element.bracket_id +'</td><td>'+ escapeHtml(element.bracket_query) +'</td><td>'+ Boolean(element.bracket_result) +'</td></tr>');
                       dataRows.appendTo(tableQueries);
                     });
               
                   queryResultContainer.html(tableQueries);
       
                 },
       
                 error:function(){
                   showModalWindow("Ошибка получения данных - обратитесь к администратору", _this);
                   return;
                 }
           });  
           
        }
    }
})();


//Send data Event Listener
$('.sendQueryBtn').click(bracketCheck.sendData);

//Send data Event Listener
$('.getQueryBtn').click(bracketCheck.getData);
